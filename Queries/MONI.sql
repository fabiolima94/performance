
-- DADOS ANALISTAS

SELECT
	"Client".name,
	"Item".item_id AS "Sentinela_ID",
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data do Pedido",
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME as "Hora do Pedido",
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalização",
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME as "Hora de Finalização",
	"Agent".agent_id AS "Investigador",
	cast( "Item".data->'antifraud_status'->>'details' AS TEXT) AS "Status",
	cast( "Item".data->'antifraud_status'->>'status' AS TEXT) AS "Status_Macro",
	cast( "Item".data->'payment'->>'total_amount' AS NUMERIC)/100 AS "Valor",
	cast( "Item".data->'antifraud_status'->>'comments' AS TEXT) AS "Comentario"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '7 days'
	and "ItemEvent".event_type = 2;


-- DADOS SCORE


select
	"Agent".agent_id AS "Investigador",
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalização",
	COUNT(CASE WHEN (cast("Item".data->'antifraud_status'->>'score' AS NUMERIC) <= 0.2
	AND "ItemEvent".event_type = 2) THEN 1 ELSE NULL END) AS "SCORE_02",
	COUNT(CASE WHEN (cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) > 0.2
	AND cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) <= 0.4
	AND "ItemEvent".event_type = 2) THEN 1 ELSE NULL END) AS "SCORE_04",
	COUNT(CASE WHEN (cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) > 0.4
	AND cast( "Item".data->'source'->'antifraud_status'->>'score' AS NUMERIC) <= 0.6
	AND "ItemEvent".event_type = 2) THEN 1 ELSE NULL END) AS "SCORE_06",
	COUNT(CASE WHEN (cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) > 0.6
	AND cast( "Item".data->'source'->'antifraud_status'->>'score' AS NUMERIC) <= 0.8
	AND "ItemEvent".event_type = 2) THEN 1 ELSE NULL END) AS "SCORE_08",
	COUNT(CASE WHEN (cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) > 0.8
	AND cast( "Item".data->'antifraud_status'->>'score' AS NUMERIC) <= 1
	AND "ItemEvent".event_type = 2) THEN 1 ELSE NULL END) AS "SCORE_10"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE >= NOW() - INTERVAL '7 days'

GROUP BY "Investigador", "Data de Finalização"
ORDER BY "Investigador", "Data de Finalização";


-- ANALISTAS


select
	id, agent_id

from
	queue_service."Agent";


-- PEDIDOS ABERTOS


SELECT
	"Item".item_id AS "Sentinela_ID",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE AS "Data",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME AS "Hora",
	"Agent".agent_id

FROM queue_service."Item"
	full outer JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	full outer JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '7 days'
	AND "ItemEvent".event_type = 4

ORDER BY "Sentinela_ID", "Data", "Hora";


-- PEDIDOS FECHADOS


SELECT
	"Item".item_id AS "Sentinela_ID",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE AS "Data",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME AS "Hora",
	"Agent".agent_id

FROM queue_service."Item"
	full outer JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	full outer JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '7 days'
	AND "ItemEvent".event_type = 5

ORDER BY "Sentinela_ID", "Data", "Hora";


-- PEDIDOS AGENDADOS


SELECT
	"Item".item_id AS "Sentinela_ID",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE AS "Data",
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME AS "Hora",
	"Agent".agent_id

FROM queue_service."Item"
	full outer JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	full outer JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '7 days'
	AND "ItemEvent".event_type = 8  ORDER BY "Sentinela_ID", "Data", "Hora";


-- ANALISTA MENSAL


SELECT
	"Client".name,
	"Item".item_id AS "Sentinela_ID",
	"ItemEvent".event_type,
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalização",
	"Agent".agent_id AS "Investigador",
	cast( "Item".data->'antifraud_status'->>'details' AS TEXT) AS "Status",
	cast( "Item".data->'antifraud_status'->>'status' AS TEXT) AS "Status_Macro",
	cast( "Item".data->'payment'->>'total_amount' AS NUMERIC)/100 AS "Valor"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '32 days'
	and "ItemEvent".event_type = 2;


-- RESUMO MENSAL


SELECT
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalizacao",
	"Agent".agent_id AS "Investigador",
	count(case when(cast("Item".data->'antifraud_status'->>'details' AS TEXT) = 'by_operator' and
	cast("Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_approved') then 1 else null end) as "Aprovados",
	count(*) - count(case when(cast("Item".data->'antifraud_status'->>'details' AS TEXT) = 'fraud_attack') then 1 else null end) as "Finalizados"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '32 days'
	and "ItemEvent".event_type = 2

GROUP BY "Investigador", "Data de Finalizacao"
ORDER BY "Investigador", "Data de Finalizacao";


-- CALCULO DE TEMPO MÉDIO DE ANÁLISE


SELECT
	"ClientTeam".name AS "Time",
	"Item".item_id AS "Sentinela_ID",
	max(CASE when("ItemEvent".event_type = 4) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end) AS "Data Abertura",
	max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end) AS "Data Fechado",
	extract(epoch from max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end)  -
	max(CASE when("ItemEvent".event_type = 4) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end))/60 AS "tempo",
	"Agent".agent_id

FROM queue_service."Item"
	full outer JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
	JOIN "ClientTeam" on "Client".client_team_id = "ClientTeam".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '20 days'
	and	"ItemEvent".event_type in (2,4,5)

GROUP BY "Time", "Sentinela_ID", agent_id
 	HAVING max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end) NOTNULL
	AND
  max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end) <
 	max(CASE when("ItemEvent".event_type = 2) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end)
	AND
	extract(epoch from max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end)  -
	max(CASE when("ItemEvent".event_type = 4) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end))/60 < 30
	AND
	extract(epoch from max(CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end)  -
	max(CASE when("ItemEvent".event_type = 4) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') else null end))/60 > 1
ORDER BY "Sentinela_ID";
