----------------------QUERY CBK MANUAL - QUEUE SERVICE-----------------------------
SELECT *
  "Client".name,
  queue_service."Item".item_id AS "Chave do Pedido",
  (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date AS "Data Entrada",
  (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date AS "Data Finalização",
  ("PaymentEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE AS "Data Incoming",
  "Payment".amount/100.00 AS "Valor",
  queue_service."Agent".agent_id AS "Investigador"

FROM  queue_service."Item"
  JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
  JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
  JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
  JOIN "Order" ON queue_service."Item".item_id = "Order".sentinela_id
  JOIN "Payment" ON "Order".id = "Payment".order_id
  JOIN "PaymentEvent" ON "Payment".id = "PaymentEvent".payment_id
  JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id
  JOIN "ReasonCode" ON "PaymentEvent".reason_code_id = "ReasonCode".id

  WHERE 1=1
  AND (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date>='2017-11-30'
  AND "PaymentEvent".payment_event_type_id = 5
  AND "AntifraudSettings".analysis_method_id = 2
  AND "Order".is_ghost_mode = FALSE -- Retira Clientes em Ghostmode
  AND "ItemEvent".event_type = 2
  AND "ReasonCode".code IN ('75', '83', '4837', '4863')
  AND sentinela_id LIKE 'IPIRANGAPINW-84e482cbcc224df4ad787b80942d0f60'

  ORDER BY "PaymentEvent".event_date ASC;
--------------------------------------------------------------------

----------------------QUERY CBK MANUAL - FULL ORDERS-----------------------------
SELECT
  "Client".name as "Cliente",
  ("full_orders".payment_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE AS "Data Incoming",
  "full_orders".sentinela_id AS "Sentinela ID",
  order_json->'source'->'customer'-> 'documents'->0->>'number' AS "CPF",
  "full_orders".order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo'AS "Data do Pedido",
  "full_orders".order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' AS "Data de Finalização",
  "full_orders".amount/100::NUMERIC as "Valor",
  "PaymentEventType".name AS "Status",
  "full_orders".queue_analyst AS "Investigador",
  "ClientTeam".name AS "Time"

  FROM full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  JOIN "Order" ON "full_orders".id="Order".id
  JOIN "PaymentEventType" ON full_orders.payment_event_type_id="PaymentEventType".id
  JOIN "PaymentEvent" ON "full_orders".payment_event_id="PaymentEvent".id
  LEFT JOIN "ReasonCode" ON "PaymentEvent".reason_code_id = "ReasonCode".id
  JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id
  JOIN "ClientTeam" ON "Client".client_team_id = "ClientTeam".id
  JOIN "OrderEventType" ON "full_orders".order_event_type_id = "OrderEventType".id

WHERE 1=1
  AND "full_orders".payment_event_type_id = 5
  AND ("full_orders".payment_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE >= '2018-01-01'
  AND "ReasonCode".code IN ('75', '83', '4837', '4863')
  AND "OrderEventType".id IN (5,6,9,10,11,12,15,16,18)
  AND "AntifraudSettings".analysis_method_id = 2

ORDER BY "Data Incoming" DESC;
--------------------------------------------------------------------