
-- PESQUISAR PEDIDOS AGENDADOS COM COMENTÁRIOS

select event_type, event_date, agent, comment

from queue_service."ItemEvent"

where event_date > '2018-01-01' AND
  event_type = 8;

-- DATA DO PEDIDO COM CHAVE, COMENTÁRIO E FINALIZAÇÃO

SELECT
  queue_service."Item".created_at as "Data do Pedido",
  queue_service."Item".item_id as "Data do Pedido",
  cast(queue_service."Item".data->'antifraud_status'->>'comments' as VARCHAR) as "Comentários",
  cast(queue_service."Item".data->'antifraud_status'->>'details' as VARCHAR) as "Finalização",
  cast(queue_service."Item".data->'antifraud_status'->>'status' as VARCHAR) as "Finalizaçãoa"

FROM queue_service."Item"
WHERE queue_service."Item".created_at >= NOW() - INTERVAL '1 days';

-- REALATÓRIO DE QUALIDADE

  -- full orders (não sendo usada)
select
  sentinela_id,
  "Client".name as "Cliente",
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data Entrada",
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME AS "Hora Entrada",
  (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data Finalização",
  (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME AS "Hora Finalização",
  rex_score as "Score",
  amount/100 as "Valor",
  "OrderEventType".name as "Status",
  "OrderEventType".details as "Status Reprovado",
  queue_analyst as "Investigador",
  cast(queue_service."Item".data->'antifraud_status'->>'comments' as VARCHAR) as "Comentários"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  join "OrderEventType" on full_orders.order_event_type_id = "OrderEventType".id
  join queue_service."Item" on full_orders.sentinela_id = queue_service."Item".item_id

WHERE
    order_event_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' >= NOW() - INTERVAL '20 DAYS' AND
    order_event_type_id IN (5, 6, 9, 10, 11, 12, 15, 16, 18);

-- queue service (sendo usada)
SELECT
  "Item".item_id AS "Sentinela_ID",
	"Client".name as "Loja",
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data do Pedido",
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME as "Hora do Pedido",
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalização",
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME as "Hora de Finalização",
  cast( "Item".data->'antifraud_status'->>'rex_score' AS TEXT) AS "Score",
  cast( "Item".data->'payment'->>'total_amount' AS NUMERIC)/100 AS "Valor",
  case when "ItemEvent".event_type = 8 THEN 'agendado' else
  concat(cast( "Item".data->'antifraud_status'->>'status' AS TEXT),' ',cast( "Item".data->'antifraud_status'->>'details' AS TEXT)) end as "Finalização",
	"Agent".agent_id AS "Investigador",
  cast( "Item".data->'antifraud_status'->>'comments' AS TEXT) AS "Comentario"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '20 days'
	and "ItemEvent".event_type in (2, 8);


-- queue service (sendo usada) - somas
SELECT
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalização",
  "Agent".agent_id AS "Investigador",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_approved' THEN 1 else null end) as "Aprovados",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_declined' and
    cast( "Item".data->'antifraud_status'->>'details' AS TEXT) = 'by_operator'THEN 1 else null end) as "Reprovados",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'canceled' THEN 1 else null end) as "Cancelado",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_declined' and
    cast( "Item".data->'antifraud_status'->>'details' AS TEXT) = 'fraud_suspect'THEN 1 else null end) as "Suspeito",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_declined' and
    cast( "Item".data->'antifraud_status'->>'details' AS TEXT) = 'fraud_confirmed'THEN 1 else null end) as "Fraude Confirmada",
  count (case when cast( "Item".data->'antifraud_status'->>'status' AS TEXT) = 'man_declined' and
    cast( "Item".data->'antifraud_status'->>'details' AS TEXT) = 'fraud_attack'THEN 1 else null end) as "Ataque",
  count (case when "ItemEvent".event_type = 8 THEN 1 else null end) as "Agendados"


FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	AND queue_service."Item".created_at >= NOW() - INTERVAL '20 days'
	and "ItemEvent".event_type in (2, 8)

GROUP BY "Data de Finalização", "Investigador"
ORDER BY "Data de Finalização" DESC

