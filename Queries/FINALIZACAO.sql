----------------------------SLA TRABALHADO+STATUS-----------------------------
/* Lógica para SLA_Trabalhado
if order_date::time > '21:00'
   order_date_corrected = order_date::date + interval '1 day'+ interval '8 hours'
else if order_date::time <'08:00'
   order_date_corrected = order_date::date + interval '8 hours'
else
   order_date_corrected = order_date

--Fórmula para cálculo
SLA_Trabalhado = order_event_date - order_date_corrected - (order_event_date::date - order_date_corrected::date)*interval '11 hours'

--Sob a condição de que order_event_date - order_date_corrected - (order_event_date::date - order_date_corrected::date)*interval '11 hours' >= 0
--que engloba order_event_date - order_date_corrected >=0
--Essa condição garante que nenhuma finalização finalizada anterior às 8:00 tenha menos que 11h de diferença em relação

********CONDIÇÕES DE CONTORNO PARA FINALIZAÇÃO NÃO ATENDIDAS*********

*/
SELECT
  "Client".name AS "Cliente",
  "ClientTeam".name,
  "Item".item_id AS "Sentinela_ID",
  (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data do Pedido",
  (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data de Finalização",
   EXTRACT(EPOCH FROM(queue_service."ItemEvent".event_date - queue_service."Item".created_at))/3600 AS "SLA",
 (CASE
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))/3600

  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date +
    interval '8 hours')::date)*interval '11 hours'))/3600
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours')) >= 0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours'))/3600
   ELSE 0 END) AS "SLA Trabalhado",
  "Agent".agent_id AS "Investigador",
  cast( "Item".data->'payment'->>'total_amount' AS NUMERIC)/100 AS "Valor",
  (CASE
   WHEN "Item".data->'antifraud_status'->>'status' LIKE 'man_approved' THEN 'Aprovado'
   WHEN "Item".data->'antifraud_status'->>'status' LIKE 'man_declined' AND "Item".data->'antifraud_status'->>'details' LIKE 'by_operator' THEN 'Reprovado'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_suspect' THEN 'Suspeito'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_confirmed' THEN 'Fraude confirmada'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_attack' THEN 'Ataque'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'canceled' THEN 'Cancelado'
   ELSE cast( "Item".data->'antifraud_status'->>'status' AS TEXT)
   END) AS "Status"

FROM queue_service."Item"
  JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
  JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
  JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE 1=1
  AND queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo'>= NOW() - INTERVAL '40 days'
  AND (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date >= '2017-11-30'
  AND "Client".is_ghost_mode = FALSE
  AND "ItemEvent".agent NOTNULL
AND "ItemEvent".event_type = 2;
------------------------------------------------------------------------------------------

-----------------------------------SLA TRABALHADO+STATUS - FULL ORDERS -----------------------------------

SELECT
  "Client".name AS "Cliente",
  sentinela_id AS "Sentinela_ID",
  (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data do Pedido",
  (full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data de Finalização",
   EXTRACT(EPOCH FROM(full_orders.order_event_date - full_orders.order_date))/3600 AS "SLA",
 (CASE
  WHEN (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))/3600

  WHEN (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date +
    interval '8 hours')::date)*interval '11 hours'))/3600
  WHEN (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours')) >= 0
    THEN EXTRACT(EPOCH FROM(((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((full_orders.order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours'))/3600
   ELSE 0 END) AS "SLA Trabalhado",
  full_orders.queue_analyst AS "Investigador",
  full_orders.amount/100 AS "Valor",

  /*(CASE
   WHEN "Item".data->'antifraud_status'->>'status' LIKE 'man_approved' THEN 'Aprovado'
   WHEN "Item".data->'antifraud_status'->>'status' LIKE 'man_declined' AND "Item".data->'antifraud_status'->>'details' LIKE 'by_operator' THEN 'Reprovado'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_suspect' THEN 'Suspeito'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_confirmed' THEN 'Fraude confirmada'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'fraud_attack' THEN 'Ataque'
   WHEN "Item".data->'antifraud_status'->>'details' LIKE 'canceled' THEN 'Cancelado'
   ELSE cast( "Item".data->'antifraud_status'->>'status' AS TEXT)
   END) AS "Status"
*/

  (CASE
   WHEN order_event_type_id IN (15,16) THEN 'Aprovado'
   WHEN order_event_type_id IN (9,10) THEN 'Reprovado'
   WHEN order_event_type_id = 11 THEN 'Suspeito'
   WHEN order_event_type_id = 12 THEN 'Fraude confirmada'
   WHEN order_event_type_id = 18 THEN 'Ataque'
   WHEN order_event_type_id IN (5,6) THEN 'Cancelado'
   ELSE null
   END) AS "Status",
  "Client".client_team_id as "Time"

FROM full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id
  --JOIN queue_service."Item" ON full_orders.sentinela_id = "Item".item_id

WHERE 1=1
  AND full_orders.order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo'>= NOW() - INTERVAL '40 days'
  AND "Client".is_ghost_mode = FALSE
  AND "AntifraudSettings".analysis_method_id = 2
AND order_event_type_id IN (5,6,9,10,11,12,15,16,18);

--------------------------------------------------------------------------------------------------------------

-----------------------------------DADOS AGRUPADOS POR CLIENTE-----------------------------------
SELECT
  "Client".name AS "Cliente",
  SUM(CASE WHEN order_event_type_id IN (13,14,15,16) THEN amount ELSE 0 END)/100.00 AS "($) Aprovado",
  SUM(CASE WHEN order_event_type_id IN (15,16) THEN amount ELSE 0 END)/100.00 AS "($) Aprovado Manual",
  SUM(CASE WHEN order_event_type_id NOT IN (1,2,3,4,17) then 1 ELSE 0 end) AS "(#) Transações",
  SUM(CASE WHEN order_event_type_id IN (13,14,15,16) THEN 1 ELSE NULL END) AS "(#) Aprovadas",
  SUM(CASE WHEN order_event_type_id IN (5, 6, 9, 10, 11, 12, 15, 16, 18) THEN 1 ELSE 0 END) AS "(#) Transações Manuais",
  SUM(CASE WHEN order_event_type_id IN (15, 16) THEN 1 ELSE 0 END) AS "(#) Aprovadas Manuais"

FROM full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id

WHERE 1=1
  AND (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date >= '2017-12-01'
  AND (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date <= '2017-12-31'
  AND "AntifraudSettings".analysis_method_id = 2

GROUP BY "Cliente"
ORDER BY "($) Aprovado" DESC;
-----------------------------------------------------------------------------------------------

------------------------------FINALIZAÇÃO+STATUS(AGENDADO INCLUSO)----------------------------
SELECT
  "Client".name AS "Cliente",
  "Item".item_id AS "Sentinela_ID",
  (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data do Pedido",
  (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data de Finalização",
  "Agent".agent_id AS "Investigador",
  (CASE
   WHEN "ItemEvent".event_type = 8 THEN 'Agendado'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'status' LIKE 'man_approved' THEN 'Aprovado'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'status' LIKE 'man_declined' AND "Item".data->'antifraud_status'->>'details' LIKE 'by_operator' THEN 'Reprovado'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'details' LIKE 'fraud_suspect' THEN 'Suspeito'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'details' LIKE 'fraud_confirmed' THEN 'Fraude confirmada'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'details' LIKE 'fraud_attack' THEN 'Ataque'
   WHEN "ItemEvent".event_type = 2 AND "Item".data->'antifraud_status'->>'details' LIKE 'canceled' THEN 'Cancelado'
   ELSE cast( "Item".data->'antifraud_status'->>'details' AS TEXT)
   END) AS "Status"

FROM queue_service."Item"
  JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
  JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
  JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
  AND queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo'>= NOW() - INTERVAL '3 days'
  AND "ItemEvent".event_type IN (2,8);
--------------------------------------------------------------------------------------------------------

-- PEDIDOS fINALIZADOS COM HORA, TIME, E DATA


select
  sentinela_id,
  "ClientTeam".name as "Time",
  "Client".name as "Cliente",
  (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data da Transacao",
  EXTRACT(HOUR FROM (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)) AS "Hora"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE
  --452 = PICHAU; 767 = CISSA; 614 = MULTIPLUS; 167 = ZEEDOG
  --client_id in (452)
    order_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' > '2017-12-20' and
    order_event_type_id IN (5, 6, 9, 10, 11, 12, 15, 16, 18);



-------------------------------------AGENDADOS SEMANA----------------------------------------------------


SELECT
  "Client".name AS "Cliente",
  "ClientTeam".name AS "Time",
  queue_service."Item".item_id AS "Sentinela ID",
  SUM (CASE WHEN "ItemEvent".event_type = 8 THEN 1 END) AS "#Agendamentos"
  --MIN(queue_service."ItemEvent".event_date) AS "Primeiro"

   FROM queue_service."Item"
     JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
     JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
     JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id
     JOIN "ClientTeam" ON "Client".client_team_id = "ClientTeam".id
WHERE 1=1
  AND queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo'>= NOW() - INTERVAL '40 days'
  AND "AntifraudSettings".analysis_method_id = 2

GROUP BY "Client".name, "Sentinela ID","Time"
HAVING SUM (CASE WHEN "ItemEvent".event_type = 8 THEN 1 END)>0
ORDER BY  "Client".name, "Sentinela ID","Time";



-- FINALIZAÇÃO POR ANALISTA POR TIME


select
  (event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data da Transacao",
  queue_service."Agent".agent_id as "Analista",
  count(*) as "Total",
  count(case WHEN "ItemEvent".event_type = 2 then 1 else null end) as "Finalizados",
  count(case WHEN client_team_id = 3 then 1 else null end) as "Boomers",
  count(case WHEN client_team_id = 4 then 1 else null end) as "Mediums",
  count(case WHEN client_team_id = 2 then 1 else null end) as "Ipiranga"

FROM queue_service."Item"
    JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
    JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
    JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE
    event_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' > '2017-12-20' and
    "ItemEvent".event_type in (2, 8)
    and agent NOTNULL

GROUP BY "Data da Transacao", "Analista"
ORDER BY "Data da Transacao";



-- TPV por mês


SELECT
  EXTRACT (YEAR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Ano",
  EXTRACT (MONTH FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Mes",
  SUM(CASE WHEN order_event_type_id IN (13,14,15,16) THEN full_orders.amount END)/100 AS "TPV",
  SUM(CASE WHEN order_event_type_id IN (15,16) THEN full_orders.amount END)/100 AS "TPV Manual",
  SUM(CASE WHEN order_event_type_id NOT IN (1,17) then 1 ELSE 0 end) AS "Totais",
  SUM(CASE WHEN order_event_type_id IN (2,3,5,6,9,10,11,12,15,16,18) THEN 1 ELSE 0 END) AS "Manuais"

FROM full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id

WHERE 1=1
  AND (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date >= '2017-12-01'
  AND "Client".is_ghost_mode = FALSE
  AND "AntifraudSettings".analysis_method_id = 2
  AND order_event_type_id NOT IN (17)
GROUP BY "Ano","Mes"
ORDER BY "Ano","Mes";
