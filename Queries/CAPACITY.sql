
-- ABÁLISES POR ANALISTA POR DIA

SELECT
	(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalizacao",
	"Agent".agent_id AS "Investigador",
	count(*) as "Analisados",
	count(case when "ItemEvent".event_type = 2 then 1 else null end) as "Finalizados"

FROM queue_service."Item"
	JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id

WHERE 1=1
	AND "ItemEvent".agent NOTNULL
	and "ItemEvent".event_type IN (2, 8)
	and (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE > '2017-12-20'

GROUP BY "Investigador", "Data de Finalizacao"
ORDER BY "Investigador", "Data de Finalizacao";


-- ANALISES POR LOJA


SELECT
	(order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data de Finalizacao",
	"Client".name AS "Cliente",
	count(*) as "Analisados"

FROM full_orders
	LEFT JOIN "Client" ON full_orders.client_id = "Client".id
	JOIN "AntifraudSettings" ON "Client".antifraud_settings_id = "AntifraudSettings".id

WHERE
	"AntifraudSettings".analysis_method_id = 2
	and (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE > '2017-12-20'
	and order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18)

GROUP BY "Cliente", "Data de Finalizacao"
ORDER BY "Cliente", "Data de Finalizacao";
