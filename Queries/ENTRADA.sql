
-- ENTRADA POR CLIENTE POR DIA TOTAL E MANUAL

With "CLIENTS" as (
  SELECT "Client".id, name, analysis_method_id, client_team_id
  FROM "Client"
    LEFT JOIN "AntifraudSettings" on "Client".antifraud_settings_id = "AntifraudSettings".id
)
SELECT
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "data",
  "ClientTeam".name as "Time",
  count(*) as "pedidos",
  count(case when(order_event_type_id IN (2, 3, 6, 10, 11, 12, 15, 16, 18)) then 1 else null end) as "pedidos_manual"

FROM full_orders
  LEFT JOIN "CLIENTS" on client_id = "CLIENTS".id
  left JOIN "ClientTeam" on "ClientTeam".id = "CLIENTS".client_team_id

WHERE order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' >= NOW() - INTERVAL '90 days'
  and "CLIENTS".analysis_method_id = 2
GROUP BY "CLIENTS".analysis_method_id, "data", "Time"
ORDER BY "data" desc;


--ENTRADA POR HORA

select
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data da Transacao",
  count(*) as "Manual",

  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=0 THEN 1 END) as "0:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=1 THEN 1 END) as "1:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=2 THEN 1 END) as "2:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=3 THEN 1 END) as "3:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=4 THEN 1 END) as "4:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=5 THEN 1 END) as "5:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=6 THEN 1 END) as "6:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=7 THEN 1 END) as "7:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=8 THEN 1 END) as "8:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=9 THEN 1 END) as "9:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=10 THEN 1 END) as "10:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=11 THEN 1 END) as "11:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=12 THEN 1 END) as "12:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=13 THEN 1 END) as "13:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=14 THEN 1 END) as "14:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=15 THEN 1 END) as "15:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=16 THEN 1 END) as "16:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=17 THEN 1 END) as "17:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=18 THEN 1 END) as "18:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=19 THEN 1 END) as "19:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=20 THEN 1 END) as "20:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=21 THEN 1 END) as "21:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=22 THEN 1 END) as "22:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=23 THEN 1 END) as "23:00"

from full_orders


WHERE
  --452 = PICHAU; 767 = CISSA; 614 = MULTIPLUS; 167 = ZEEDOG
  --client_id in (452)
    order_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' > '2017-12-20' and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18)

GROUP BY "Data da Transacao"
ORDER BY "Data da Transacao";


-- ENTRADA POR CLIENTE POR HORA


select
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data da Transacao",
  count(*) as "Manual",
  "ClientTeam".name as "Time",
  "Client".name as "Cliente",

  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=0 THEN 1 END) as "0:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=1 THEN 1 END) as "1:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=2 THEN 1 END) as "2:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=3 THEN 1 END) as "3:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=4 THEN 1 END) as "4:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=5 THEN 1 END) as "5:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=6 THEN 1 END) as "6:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=7 THEN 1 END) as "7:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=8 THEN 1 END) as "8:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=9 THEN 1 END) as "9:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=10 THEN 1 END) as "10:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=11 THEN 1 END) as "11:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=12 THEN 1 END) as "12:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=13 THEN 1 END) as "13:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=14 THEN 1 END) as "14:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=15 THEN 1 END) as "15:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=16 THEN 1 END) as "16:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=17 THEN 1 END) as "17:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=18 THEN 1 END) as "18:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=19 THEN 1 END) as "19:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=20 THEN 1 END) as "20:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=21 THEN 1 END) as "21:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=22 THEN 1 END) as "22:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=23 THEN 1 END) as "23:00"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE
  --452 = PICHAU; 767 = CISSA; 614 = MULTIPLUS; 167 = ZEEDOG
  --client_id in (452)
    order_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' > '2017-12-20' and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18)

GROUP BY "Data da Transacao",  "Time", "Cliente"
ORDER BY "Data da Transacao", "Time", "Cliente" ;


-- PEDIDOS ENTRANDO COM HORA, TIME, E DATA


select
  sentinela_id,
  "ClientTeam".name as "Time",
  "Client".name as "Cliente",
  (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE as "Data da Transacao",
  EXTRACT(HOUR FROM (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -
	(order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)) AS "Hora"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE
  --452 = PICHAU; 767 = CISSA; 614 = MULTIPLUS; 167 = ZEEDOG
  --client_id in (452)
    order_date::DATE AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' > '2017-12-20' and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18);



----------------------------------------TRX/CPF-------------------------------------------------
SELECT
document_number AS CPF,
count(1) AS trx
FROM full_orders

WHERE 1=1
 AND order_event_type_id IN (2,3)
 AND client_id = 614 --Multiplus
GROUP BY CPF
ORDER BY trx DESC;
------------------------------------------------------------------------------------------------