----------------------------SLA TRABALHADO+STATUS-----------------------------
/* Lógica para SLA_Trabalhado
if order_date::time > '21:00'
   order_date_corrected = order_date::date + interval '1 day'+ interval '8 hours'
else if order_date::time <'08:00'
   order_date_corrected = order_date::date + interval '8 hours'
else
   order_date_corrected = order_date

--Fórmula para cálculo
SLA_Trabalhado = order_event_date - order_date_corrected - (order_event_date::date - order_date_corrected::date)*interval '11 hours'

--Sob a condição de que order_event_date - order_date_corrected - (order_event_date::date - order_date_corrected::date)*interval '11 hours' >= 0
--que engloba order_event_date - order_date_corrected >=0
--Essa condição garante que nenhuma finalização finalizada anterior às 8:00 tenha menos que 11h de diferença em relação

********CONDIÇÕES DE CONTORNO PARA FINALIZAÇÃO NAÃO ATENDIDAS**********/

-- SLA TRABALHADO META

SELECT
  "ClientTeam".name,
  (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') AS "Data de Finalização",
   EXTRACT(EPOCH FROM(queue_service."ItemEvent".event_date - queue_service."Item".created_at))/3600 AS "SLA",
 (CASE
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))/3600

  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time > '21:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '1 day'+ interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '1 day'+ interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))>=0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date +
    interval '8 hours')::date)*interval '11 hours'))/3600
  WHEN (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::time < '08:00'
    AND EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date + interval '8 hours'))
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - ((queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    + interval '8 hours')::date)*interval '11 hours'))<0
    THEN 0
  WHEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours')) >= 0
    THEN EXTRACT(EPOCH FROM(((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')
    - ((queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date
    - (queue_service."Item".created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date)*interval'11 hours'))/3600
   ELSE 0 END) AS "SLA Trabalhado"

FROM queue_service."Item"
  JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
  JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE 1=1
  AND EXTRACT (MONTH FROM(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')) = EXTRACT(MONTH FROM(NOW()))
  AND "Client".is_ghost_mode = FALSE
  AND "ItemEvent".agent NOTNULL

AND "ItemEvent".event_type = 2;

-- FINALIZAÇÃO POR DIA MES CORRENTE


SELECT
  (queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::Date AS "Data do Pedido",
  count(*) as "Geral",
  count(case when client_team_id = 2 then 1 else null end) as "Ipiranga",
  count(case when client_team_id = 3 then 1 else null end) as "Boomers",
  count(case when client_team_id = 4 then 1 else null end) as "Mediums"

FROM queue_service."Item"
  JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
  JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key

WHERE 1=1
  AND EXTRACT (MONTH FROM(queue_service."ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')) = EXTRACT(MONTH FROM(NOW()))
  AND "Client".is_ghost_mode = FALSE
  AND "ItemEvent".agent NOTNULL
  AND "ItemEvent".event_type = 2

GROUP BY "Data do Pedido"
ORDER BY "Data do Pedido";

-- INDICADORES DO DIA

SELECT
  "ClientTeam".name as "Time",
  count(case when (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date
    and order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18) then 1 else null end) as "Entrada",
  count(case when (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date and
    order_event_type_id IN (5, 6, 9, 10, 11, 12, 15, 16, 18) then 1 else null end) as "Finalizacao",
  count(case when order_event_type_id IN (2, 3) then 1 else null end) as "Fila",
  count(CASE WHEN (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date AND
    order_event_type_id IN (15, 16) then 1 else null end) as "Aprovados",
  count(case when (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date then 1 else null end) as "Total Entrada",
  count(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')>=21 AND
    (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = (now()::date - 1) and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18) THEN 1 else null END) as "Depois 21",
  count(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')<8 AND
    (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = (now()::date) and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18) THEN 1 else null END) as "Antes 8"

FROM full_orders
  join "Client" on full_orders.client_id = "Client".id
  JOIN "AntifraudSettings" on "Client".antifraud_settings_id = "AntifraudSettings".id
  JOIN "ClientTeam" on "Client".client_team_id = "ClientTeam".id

WHERE 1=1
  and analysis_method_id = 2
  and order_date >= now() - INTERVAL '5 days'

GROUP BY "Time";


-- ENTRADA POR HORA

select
  "ClientTeam".name as "Time",

  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=0 THEN 1 END) as "0:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=1 THEN 1 END) as "1:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=2 THEN 1 END) as "2:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=3 THEN 1 END) as "3:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=4 THEN 1 END) as "4:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=5 THEN 1 END) as "5:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=6 THEN 1 END) as "6:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=7 THEN 1 END) as "7:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=8 THEN 1 END) as "8:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=9 THEN 1 END) as "9:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=10 THEN 1 END) as "10:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=11 THEN 1 END) as "11:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=12 THEN 1 END) as "12:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=13 THEN 1 END) as "13:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=14 THEN 1 END) as "14:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=15 THEN 1 END) as "15:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=16 THEN 1 END) as "16:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=17 THEN 1 END) as "17:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=18 THEN 1 END) as "18:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=19 THEN 1 END) as "19:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=20 THEN 1 END) as "20:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=21 THEN 1 END) as "21:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=22 THEN 1 END) as "22:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=23 THEN 1 END) as "23:00"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE
    (order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date and
    order_event_type_id IN (2, 3, 5, 6, 9, 10, 11, 12, 15, 16, 18)

GROUP BY "Time"
ORDER BY "Time";


-- FINALIZAÇÃO POR HORA


select
  "ClientTeam".name as "Time",

  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=0 THEN 1 END) as "0:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=1 THEN 1 END) as "1:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=2 THEN 1 END) as "2:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=3 THEN 1 END) as "3:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=4 THEN 1 END) as "4:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=5 THEN 1 END) as "5:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=6 THEN 1 END) as "6:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=7 THEN 1 END) as "7:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=8 THEN 1 END) as "8:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=9 THEN 1 END) as "9:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=10 THEN 1 END) as "10:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=11 THEN 1 END) as "11:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=12 THEN 1 END) as "12:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=13 THEN 1 END) as "13:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=14 THEN 1 END) as "14:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=15 THEN 1 END) as "15:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=16 THEN 1 END) as "16:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=17 THEN 1 END) as "17:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=18 THEN 1 END) as "18:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=19 THEN 1 END) as "19:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=20 THEN 1 END) as "20:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=21 THEN 1 END) as "21:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=22 THEN 1 END) as "22:00",
  SUM(CASE WHEN EXTRACT(HOUR FROM order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')=23 THEN 1 END) as "23:00"

from full_orders
  JOIN "Client" ON full_orders.client_id = "Client".id
  left JOIN "ClientTeam" on "ClientTeam".id = "Client".client_team_id

WHERE
    (order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::date = now()::date and
    order_event_type_id IN (5, 6, 9, 10, 11, 12, 15, 16, 18)

GROUP BY "Time"
ORDER BY "Time";


SELECT
  date_trunc('day', order_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') as hour_chegada,
  date_trunc('day', order_event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') as hour_finalizacao,
  sum(full_orders.amount)/100 as "Valor",

  CASE
    WHEN order_event_type_id IN (2,3) THEN 'Em Fila'
    WHEN order_event_type_id IN (15,16) THEN 'Aprovado'
    WHEN order_event_type_id IN (10) THEN 'Reprovado'
    WHEN order_event_type_id IN (11) THEN 'Suspeito'
    WHEN order_event_type_id IN (12) THEN 'Fraude'
    WHEN order_event_type_id IN (18) THEN 'Ataque'
    WHEN order_event_type_id IN (5,6) THEN 'Cancelado'
    ELSE 'Erro'
  END AS status,

  "ClientTeam".name AS client_team,
  count(1) AS num_trx

FROM full_orders
  JOIN "Client" ON client_id = "Client".id
  JOIN "ClientTeam" ON "Client".client_team_id = "ClientTeam".id
WHERE order_date > (current_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo') - INTERVAL '35 Days'
  AND order_event_type_id IN (9,10,11,12,18,15,16,2,3,5,6)

GROUP BY hour_chegada, client_team, hour_finalizacao, status;