
-- PESQUISAR JSON DO PEDIDO (SENTINELA-ID)

select
  sentinela_id,
  order_json

from "Order"

where
  sentinela_id = 'CISSAMAGAZINE-0aa08c9593b846d9a6e71ddef07d1086';


-- PESQUISAR JSON DO PEDIDO (EXTERNAL-ID)


select
 external_id,
 order_json

from "Order"

where
 external_id = '5a98e7a0-9855-4b15-9ace-319f21990900';


-- PESQUISAR VIDA DO PEDIDO QUEUE SERVICE


SELECT
	"ClientTeam".name AS "Time",
	"Item".item_id AS "Sentinela_ID",
	CASE
		WHEN "ItemEvent".event_type = 2 THEN 'FINALIZADO'
		WHEN "ItemEvent".event_type = 4 THEN 'ABERTO'
		WHEN "ItemEvent".event_type = 5 THEN 'FECHADO'
		ELSE NULL END AS "STATUS",
	CASE when("ItemEvent".event_type = 4) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE else null end AS "Data Abertura",
	CASE when("ItemEvent".event_type = 5) then ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE else null end AS "Data Fechado",
	CASE when("ItemEvent".event_type = 4) then
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -    ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME
	else null end AS "Hora Aberto",
	CASE when("ItemEvent".event_type = 5) then
	("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo' -    ("ItemEvent".event_date AT TIME ZONE 'UTC' AT TIME ZONE 'America/Sao_Paulo')::DATE)::TIME
	else null end AS "Hora Fechado",
	"Agent".agent_id

FROM queue_service."Item"
	full outer JOIN queue_service."ItemEvent" ON queue_service."Item".id = queue_service."ItemEvent".item_id
	JOIN queue_service."Agent" ON queue_service."ItemEvent".agent = queue_service."Agent".id
	JOIN "Client" ON cast( "Item".data->>'client_key' AS text) = "Client".client_key
	JOIN "ClientTeam" on "Client".client_team_id = "ClientTeam".id

WHERE 1=1
AND "ItemEvent".agent NOTNULL
AND queue_service."Item".created_at >= NOW() - INTERVAL '20 days'
AND "Item".item_id = 'CISSAMAGAZINE-0aa08c9593b846d9a6e71ddef07d1086'

ORDER BY "Sentinela_ID";