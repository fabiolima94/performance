

-- BUSCA CLIENTES MANUAIS RAIZ


select "Client".id, client_key, "ClientTeam".name, "Client".name

from "Client"
  join "AntifraudSettings" on "Client".antifraud_settings_id = "AntifraudSettings".id
  left join "ClientTeam" on client_team_id = "ClientTeam".id

where analysis_method_id = 2;